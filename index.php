<?php
   header('content-type: application/json;'); 
   header('Access-Control-Allow-Origin: *');  
   include './class.conexion.php';
   $claseConexion = new Conexion();
   $conexion =  $claseConexion->conexion();
   $rows = null;
   $sql = "SELECT * FROM libro";
   mysqli_set_charset($conexion,"utf8");
   $query = $conexion->query( $sql );
   while($resultado = $query->fetch_array()){
       $rows[] = $resultado;
   }
   echo json_encode($rows);
?>